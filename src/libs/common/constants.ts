export const ALGORITHM = "sha512";
export const ENCODING_CRYPTO = "hex";
export const QUERY_DATA_CRYPTO = "key=value";
export const ENCODING_CRYPTO_FORMAT = "utf8";

export const GENERIC_ERROR_CODE = "0031";
export const GENERIC_ERROR_MESSAGE = "Código de respuesta inválido.";
