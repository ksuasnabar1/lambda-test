export interface GcsStatusCodesEntity {
  qikStatusId: number;
  gcsStatusId: string;
  qikStatusDetail: string;
}
