export const ALGORITHM = "sha512";
export const ENCODING_CRYPTO = "hex";
export const QUERY_DATA_CRYPTO = "key=value";
export const ENCODING_CRYPTO_FORMAT = "utf8";
export const DEV_LOCAL = "DEV_LOCAL";
