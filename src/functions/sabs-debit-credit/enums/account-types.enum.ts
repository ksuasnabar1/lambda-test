export enum ACCOUNT_TYPES {
  SAV = "SAV",
  DDA = "DDA",
  CC = "CC",
  LOAN = "LOAN",
}
