export class SabsResponseStatusCodeDto {
  constructor(
    readonly code: string,
    readonly description: string,
  ) {}
}
