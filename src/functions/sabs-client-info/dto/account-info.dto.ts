export class AccountInfo {
  id: string;
  userId: string;
  status: boolean;
  type: string;
  reference: string;
}
