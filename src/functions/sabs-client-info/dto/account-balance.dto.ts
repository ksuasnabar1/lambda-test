export class AccountBalanceDto {
  availableBalance: number;
  availableFunds: number;
  onlineActualBalance: number;
}
