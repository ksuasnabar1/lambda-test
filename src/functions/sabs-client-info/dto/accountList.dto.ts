import { AccountDto } from "./account.dto";

export class AccountListDto {
  accountList: AccountDto[];
}
