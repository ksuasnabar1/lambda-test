export class UserDetailParsedResponseDto {
  customerFullName: string;
  address: string;
  status: string;
  city: string;
  segment?: string;
  officerCode?: string;
  officerName?: string;
  email: string;
  gender: string;
  dateOfBirth: string;
}
