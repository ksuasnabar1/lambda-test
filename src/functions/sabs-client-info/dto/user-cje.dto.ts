export class UserJCEDto {
  firstName: string;
  lastName1: string;
  lastName2: string;
  sex: string;
  bornDate: string;
}
