export class AddressDto {
  principal: boolean;
  street: string;
  buildingNumber: string;
  building: string;
  district: string;
  town: string;
}
