export const BANK_ID = "1129";
export const BANK_NAME = "QIK";

export const TYPE_ACCOUNT = {
  SAV: "SAV",
  DDA: "DDA",
  CC: "CC",
  LOAN: "LOAN",
};

export const CURRENCY = {
  DOP: "DOP",
  USD: "USD",
};

export const PRODUCT_SERVICE_HMAC_SECRET_ID = "product-service-hmac";
export const PRODUCT_SERVICE_HMAC_KEY = "PRODUCT_SERVICE_HMAC_KEY";

export const USER_SERVICE_HMAC_SECRET_ID = "user-service-hmac";
export const USER_SERVICE_HMAC_KEY = "USER_SERVICE_HMAC_KEY";
